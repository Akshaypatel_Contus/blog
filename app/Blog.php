<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
	protected $table = 'blogs';
    protected $fillable = [
        'blog_title', 'blog_desc', 'blog_img',
    ];
}
