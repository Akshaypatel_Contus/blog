<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ApiController extends Controller
{
	 public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function blog_insert(Request $request)
    {
        $validatedData = $request->validate([
            'blog_title' => 'required|max:255',
            'blog_desc' => 'required',
            'blog_img' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:1024',
        ]);
        $blog_title=$request->input('blog_title');
        $blog_desc=$request->input('blog_desc');

        $path = $request->blog_img->store('app');
        $blog=new myblogs;
        $blog->blog_title=$blog_title;
        $blog->blog_desc=$blog_desc;
        $blog->blog_img=$path;
        $blog->save();
        return response()->json($blog, 201);
        //return $blog;
        //return view('home');
    }
    public function blogs()
    {
        $blog=myblogs::paginate(3);
        return $blog;
        //return view('blogs')->with('blog',$blog);
    }
    public function deleteblog(Request $req)
    {
        if($req->input('id')!==null)
        {
            $id=$req->input('id');
            $blog=myblogs::destroy($id);
            return response()->json($blog, 201);
            //return $blog;
            //return redirect("/home");
        }
        
    }
    public function editblog(Request $req)
    {
        if($req->input('id')!==null)
        {
            
            $id=$req->input('id');
            $blog=myblogs::all()->where('id','=',$id);
            return $blog;
            //return view('updateblogs')->with('blog',$blog);
        }
        
    }
    public function blog_update(Request $req)
    {
        if($req->input('id')!==null)
        {
            $id=$req->input('id');
            $validatedData = $req->validate([
                    'blog_title' => 'required|max:255',
                    'blog_desc' => 'required',
            ]);
            $blog=myblogs::find($id);
            if($req->input('blog_title')!==null)
            {
                $blog->blog_title = $req->input('blog_title');
            }
            if($req->input('blog_desc')!==null)
            {
                $blog->blog_desc = $req->input('blog_desc');  
            }
            if($req->file('blog_img')!==null)
            {
                $path = $req->blog_img->store('app');
                $blog->blog_img = $path;     
            }
            $blog->save();
            return response()->json($blog, 201);
            //return redirect("/blog");
        }
    }
    
    public function insertuser(Request $req)
    {
        $validatedData = $req->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
        $user=new User;
        $user->name=$req->input('name');
        $user->email=$req->input('email');
        $user->password=bcrypt($req->input('password'));
        $user->save();
    }
    public function index()
    {

    	$blog=myblogs::paginate(3);
    	
    	//$blog=myblogs::all()->offset(0)->limit(1);
    	//print_r($blog);
    	return $blog;
    }
}
