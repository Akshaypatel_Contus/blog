<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\myblogs;
use App\User;
use Auth;
use App\Blogcomment;
use App\Replycmt;
use App\Gallery;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }
    public function blog_insert(Request $request)
    {
        $validatedData = $request->validate([
            'blog_title' => 'required|max:255',
            'blog_desc' => 'required',
            'blog_img' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:1024',
        ]);
        $blog_title=$request->input('blog_title');
        $blog_desc=$request->input('blog_desc');

        $path = $request->blog_img->store('app');
        $blog=new myblogs;
        $blog->blog_title=$blog_title;
        $blog->blog_desc=$blog_desc;
        $blog->blog_img=$path;
        $blog->save();
        return view('home');
    }
    public function blogs()
    {
        $blog=myblogs::orderBy('updated_at', 'desc')->paginate(3);
        return view('blogs')->with('blog',$blog);
    }
    public function deleteblog(Request $req)
    {
        if($req->input('id')!==null)
        {
            $id=$req->input('id');
            $blog=myblogs::destroy($id);
            return redirect("/home");
        }
        else
        {
            return redirect("/home");
        }
    }
    public function editblog(Request $req)
    {
        if($req->input('id')!==null)
        {
            
            $id=$req->input('id');
            $blog=myblogs::all()->where('id','=',$id);
            return view('updateblogs')->with('blog',$blog);
        }
        else
        {
            return redirect("/home");
        }
    }
    public function blog_update(Request $req)
    {
        if($req->input('id')!==null)
        {
            $id=$req->input('id');
            $validatedData = $req->validate([
                    'blog_title' => 'required|max:255',
                    'blog_desc' => 'required',
            ]);
            $blog=myblogs::find($id);
            if($req->input('blog_title')!==null)
            {
                $blog->blog_title = $req->input('blog_title');
            }
            if($req->input('blog_desc')!==null)
            {
                $blog->blog_desc = $req->input('blog_desc');  
            }
            if($req->file('blog_img')!==null)
            {
                $path = $req->blog_img->store('app');
                $blog->blog_img = $path;     
            }
            $blog->save();
            return redirect("/blog");
        }
    }
    public function adduser()
    {
        return view('adduser');
    }
    public function insertuser(Request $req)
    {
        $validatedData = $req->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
        $user=new User;
        $user->name=$req->input('name');
        $user->email=$req->input('email');
        $user->password=bcrypt($req->input('password'));
        $user->save();
        return redirect("/home");
    }
    public function profile()
    {
        $name = Auth::user()->name;
        $email= Auth::user()->email;
        //$userprofile=User::find($user);
        //print_r($name.$email);exit;
        $profile=array();
        $profile['name']=$name;
        $profile['email']=$email;
        return view('profile')->with('user',$profile);
    }
    public function editprofile()
    {
        $name = Auth::user()->name;
        $email= Auth::user()->email;
        $profile=array();
        $profile['name']=$name;
        $profile['email']=$email;
        return view('updateprofile')->with('user',$profile);
    }
    public function update_profile(Request $req)
    {
        $validatedData = $req->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
        ]);
        $user=Auth::user();
        $user->name = $req->input('name');
        $user->email = $req->input('email');
        $user->save();
        $name = Auth::user()->name;
        $email= Auth::user()->email;
        $profile=array();
        $profile['name']=$name;
        $profile['email']=$email;
        return redirect('editprofile')->with('user',$profile);
    }
    public function multiupload()
    {
        return view('multiupload');
    }
    public function multi_upload_insert(Request $req)
    {
        $validatedData = $req->validate([
            'name' => 'required|string|max:255',
            'images' => 'required',
            'images.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);
        $name=$req->input('name');
        $nameoffile='';
        $gallery = new Gallery;
        $count = count($req->file('images'));
        //print_r($count);exit;
        $i=1;
        foreach ($req->images as $photo) {
            if($i==$count)
            {
                $filename = $photo->store('app');
                $nameoffile=$nameoffile.$filename;
            }
            else
            {
                $filename = $photo->store('app');
                $nameoffile=$nameoffile.$filename.",";
            }
            $i++;
            
        }
        $gallery->name=$name;
        $gallery->image=$nameoffile;
        $gallery->save();
        return redirect('multiupload');
    }
    public function viewgallery()
    {
        $gallery=Gallery::all();
        return view('viewgallery')->with('gallery',$gallery);
    }
    public function blog_view(Request $req)
    {
        $id=$req->input('id');
        if($id!==null)
        {
            $blog = myblogs::where('id', $id)->get();
            return view('blog_view')->with('blog',$blog);
        }
        else
        {
            return redirect('blog');
        }
    }
    public function load_comment(Request $req)
    {
        $id=$req->input('id');
        $blog_comment=Blogcomment::all()->where('blog_id','=',$id);
        return response()->json($blog_comment);
    }
    public function blog_comment(Request $req)
    {
        $blog_id=$req->input('id');
        $comment=$req->input('comment');
        $email= Auth::user()->email;
        $blog_comment=new Blogcomment;
        $blog_comment->blog_id=$blog_id;
        $blog_comment->email=$email;
        $blog_comment->comment=$comment;
        $blog_comment->save();
        return response()->json('success');

    }
    public function reply_comment(Request $req)
    {
        $cmt_id=$req->input('id');
        $email= Auth::user()->email;
        $cmt=$req->input('cmt');
        $replycmt=new Replycmt;
        $replycmt->cmt_id=$cmt_id;
        $replycmt->email=$email;
        $replycmt->replycmt=$cmt;
        $replycmt->save();
        return response()->json('success');
    }
    public function load_reply(Request $req)
    {
        $id=$req->input('id');
        $replycmt=Replycmt::all()->where('cmt_id','=',$id);
        return response()->json($replycmt);
    }
    public function delete_reply(Request $req)
    {
        $id=$req->input('id');
        $replycmt=Replycmt::destroy($id);
        return response()->json('success');   
    }
    public function delete_cmt(Request $req)
    {
        $id=$req->input('id');
        $blog_comment=Blogcomment::destroy($id);
        $replycmt=Replycmt::where('cmt_id','=',$id)->delete();
        return response()->json('success');
    }
}
