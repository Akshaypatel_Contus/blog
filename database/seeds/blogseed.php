<?php

use Illuminate\Database\Seeder;
use App\myblogs;

class blogseed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $faker = \Faker\Factory::create();
         for ($i = 0; $i < 50; $i++) {
            myblogs::create([
                'blog_title' => $faker->sentence,
                'blog_desc' => $faker->paragraph,
                'blog_img'=> "app/WSMgwnbPZ92d73pU5vFJVsENYo3Tx7LECymKm4if.png",
            ]);
        }
    }
}
