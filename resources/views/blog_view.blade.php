
<meta name="csrf-token" content="{{ csrf_token() }}">
@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        @include('layouts.leftside')
        <div class="col-md-8">
            <?php $id; ?>
            @foreach($blog as $key)
            <?php $id=$key->id; ?>
            <div class="panel panel-default">
                <div class="panel-heading">{{ $key->blog_title }}</div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-12">{{ $key->blog_desc }}</div>    
                    </div>
                    <div class="row">
                        
                        <div class="col-sm-6">
                            <img src="{{ asset($key->blog_img) }}" />
                        </div>
                        
                    </div>
                    @endforeach
                </div>    
            </div>
            
            <textarea class="form-control" rows="5" id="cmt" placeholder="Your Comment"></textarea>
            <br/>
            <button class="btn btn-success btn-md pull-right" id="comment" ng-click=''>Comment</button>
            <br/>
            <hr/>
            <h3>Comments</h3>
            <div class="list-group" id="blog_comment">
                
            </div>
       
        </div>
    </div>
</div>
<script src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script type="text/javascript">
    $.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});
    $(document).ready(function(){
        loadcomments();
    $('#comment').click(function(){
        var comment=$('#cmt').val();
        var id=<?php echo $id; ?>;
        //alert(comment);
        $.ajax({
            url:'/blog_comment',
            type:'post',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data:{comment:comment,id:id},
            success:function(data){
                //alert('hello');
                $('#cmt').val('');
                loadcomments();
           }
        });
    });
});
    function loadcomments()
    {
        var id=<?php echo $id; ?>;
        var email='{{ Auth::user()->email }}';
        $.ajax({
            url:'load_comment',
            type:'post',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data:{id:id},
            success:function(data){
                $('#blog_comment').html('');
                $.each(data, function(i, field){
                    if(field['email']==email)
                    {
                        $("#blog_comment").append("<div class='row' id='commentdiv"+field['id']+"'><h3 class='row well'>"+field['email']+"<div style='float:right;'><button type='button' id='delete' class='btn btn-danger'>Delete</button></div></h3><div class='row'><div class='col-sm-6' style='color:blue;'><p>"+field['comment']+"</p></div></div>");   

                        $("#blog_comment").append("<script>$('#delete').click(function(){ var cmt_id="+field['id']+"; $.ajax({ url:'delete_cmt',type:'post',headers: { 'X-CSRF-TOKEN': $('meta[name=\"csrf-token\"]').attr('content') },data:{id:cmt_id},success:function(data){loadcomments();} });   });<\/script>");
                    }
                    else
                    {
                        $("#blog_comment").append("<div class='row' id='commentdiv"+field['id']+"'><h3 class='row well'>"+field['email']+"</h3><div class='row'><div class='col-sm-6' style='color:blue;'><p>"+field['comment']+"</p></div></div>");
                    }
                    var id1=field['id'];
                    
                    $.ajax({
                        url:'load_reply',
                        type:'post',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        data:{id:id1},
                        success:function(data){
                            $.each(data, function(i, field1){
                                if(field1['email']==email)
                                {
                                    $("#commentdiv"+field['id']+"").append("<div class='row' style='float:right;width:100%;'><div class='col-sm-6'></div><div class='col-sm-6'><div class='row'>"+field1['email']+"</div><div class='row'><p style='color:blue;'>"+field1['replycmt']+"</p><button type='button' id='reply_delete"+field1['id']+"' class='btn btn-danger'>Delete</button></div></div></div>");

                                    $("#commentdiv"+field['id']+"").append("<script>$('#reply_delete"+field1['id']+"').click(function(){var rep_id="+field1['id']+"; $.ajax({ url:'delete_reply',type:'post',headers: { 'X-CSRF-TOKEN': $('meta[name=\"csrf-token\"]').attr('content') },data:{id:rep_id},success:function(data){loadcomments();} });   });<\/script>");
                                }
                                else
                                {
                                    $("#commentdiv"+field['id']+"").append("<div class='row' style='float:right;width:100%;'><div class='col-sm-6'></div><div class='col-sm-6'><div class='row'>"+field1['email']+"</div><div class='row' style='color:blue;'><p>"+field1['replycmt']+"</p></div></div></div>");   
                                }
                            });
                                
                        }
                    });
                    


                    $("#commentdiv"+field['id']+"").append("<button id='reply"+field['id']+"' class='btn btn-warning' style='float:right;'>Reply</button>");


                    $("#commentdiv"+field['id']+"").append("<div class='row' id='reply-div"+field['id']+"' style='display:none;width:100%;'><div class='row'><textarea id='reply_comment"+field['id']+"' style='float:right;'></textarea></div><div class='row forn-control'><button class='btn btn-success' type='button' style='float:right;' id='reply_button"+field['id']+"'>Reply</button></div></div>");


                    $("#commentdiv"+field['id']+"").append("<script>$('#reply"+field['id']+"').click(function(){ $('#reply-div"+field['id']+"').css('display','block');$('#reply"+field['id']+"').css('display','none');});<\/script>");


                    $("#commentdiv"+field['id']+"").append("<script>$('#reply_button"+field['id']+"').click(function(){ var id="+field['id']+"; var cmt=$('#reply_comment"+field['id']+"').val();var CSRF_TOKEN = '{{csrf_token()}}'; $.ajax({ url:'reply_comment',type:'post',headers: {'X-CSRF-TOKEN': CSRF_TOKEN },data:{id:id,cmt:cmt},success:function(data){$('#reply_comment').val('');loadcomments();} });});<\/script>");
                    $('#blog_comment').append('</div>')
                });
           }
        });
    }

    
</script>
@endsection