@extends('layouts.app')

@section('content')
<?php use Illuminate\Support\Facades\Storage; ?>
<div class="container">
    <div class="row">
    	@include('layouts.leftside')
        <div class="col-md-8">
            
                    
                    @foreach($blog as $key)
            <div class="panel panel-default">
                <div class="panel-heading">{{ $key->blog_title }}</div>

                <div class="panel-body">
                    
                    <div class="row">
                        <div class="col-sm-6">
  							{{ $key->blog_desc }}
                            <a href="{{ route('blog_view') }}?id={{ $key->id }}">View More</a>
                        </div>
                    </div><br/>
                    <div class="row">
                        
                        	<?php 
                        	//Storage::disk('local')->put('avatars/filename.jpg', $file);
                        	//$file=Storage::disk('local')->url("app/".$key->blog_img);
                        	$path = storage_path($key->blog_img);
                        	$url = Storage::url($key->blog_img);
                        	 ?>
  							<!--<img src="{{ asset($key->blog_img) }}" />-->
                        
                    </div><br/>
                    <div class="row">
                        <div class="col-sm-4">Created At:<br/>{{ $key->created_at }}</div>
                        <div class="col-sm-4">Updated_at:<br/>{{ $key->updated_at }}</div>
                        <div class="col-sm-4">
                            <div class="col-sm-6"><a class="btn btn-danger" href="<?php echo url('delete').'?id='.$key->id; ?>">Delete</a></div>
                            <div class="col-sm-6"><a class="btn btn-primary" href="<?php echo url('edit').'?id='.$key->id; ?>">Edit</a></div>
                        </div>
                    </div>
                </div>
            </div>
                    @endforeach
                    <?php echo $blog->render(); ?>
                
        </div>
    </div>
</div>

@endsection