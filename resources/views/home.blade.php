@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        @include('layouts.leftside')
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form action="{{ route('blog_insert') }}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                    <div class="row">
                        <div class="col-sm-6">
                            Blog Title:
                        </div>
                        <div class="col-sm-6">
                            <input type="text" name="blog_title" class="form-control" />
                            @if ($errors->has('blog_title'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('blog_title') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div><br/>
                    <div class="row">
                        <div class="col-sm-6">
                            Blog Description
                        </div>
                        <div class="col-sm-6">
                            <textarea name="blog_desc" class="form-control"></textarea>
                            @if ($errors->has('blog_desc'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('blog_desc') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div><br/>
                    <div class="row">
                        <div class="col-sm-6">Blog Image Attachment</div>
                        <div class="col-sm-6">
                            <input type="file" name="blog_img" />
                            @if ($errors->has('blog_img'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('blog_img') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6"></div>
                        <div class="col-sm-6">
                            <button type="submit" name="submit" class="btn btn-primary">Add My Blog</button>
                        </div>
                    </div>
                </form>

                   


                </div>
            </div>
        </div>
    </div>
</div>
@endsection
