@extends('layouts.app')

@section('content')
<?php use Illuminate\Support\Facades\Storage; ?>
<div class="container">
    <div class="row">
    	<div class="col-sm-2"></div>
        <div class="col-md-8">
            
                    
                    @foreach($blog as $key)
            <div class="panel panel-default">
                <div class="panel-heading">{{ $key->blog_title }}</div>

                <div class="panel-body">
                    
                    <div class="row">
                        <div class="col-sm-6">
  							{{ $key->blog_desc }}
                        </div>
                    </div><br/>
                    <div class="row">
                        <div class="col-sm-6">
                        	<?php 
                        	//Storage::disk('local')->put('avatars/filename.jpg', $file);
                        	//$file=Storage::disk('local')->url("app/".$key->blog_img);
                        	$path = storage_path($key->blog_img);
                        	$url = Storage::url($key->blog_img);
                        	 ?>
  							<img src="{{ asset($key->blog_img) }}" />
                        </div>
                    </div><br/>
                    <div class="row">
                        <div class="col-sm-6">{{ $key->created_at }}</div>
                        <div class="col-sm-6">{{ $key->updated_at }}

                        </div>
                    </div>
                </div>
            </div>
                    @endforeach
                    <?php echo $blog->render(); ?>
                
        </div>
    </div>
</div>

@endsection