@extends('layouts.app')

@section('content')
<?php use Illuminate\Support\Facades\Storage; ?>
<div class="container">
    <div class="row">
    	@include('layouts.leftside')
        <div class="col-md-8">
            
            <div class="panel panel-default">
                <div class="panel-heading">Multi File Upload</div>
                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('multi_upload_insert') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Gallery Name</label>
                            <div class="col-md-6">
                                <input id="" type="name" class="form-control" name="name" value="" required autofocus>
                                 @if ($errors->has('image'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif

                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Files</label>
                            <div class="col-md-6">
                                <input id="images" type="file" class="form-control" name="images[]" multiple value="" required autofocus>
                                 @if ($errors->has('images'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('images') }}</strong>
                                    </span>
                                @endif

                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-success" value="Upload My Photos">
                                    Update
                                </button>
                            </div>
                        </div>
                    </form>
                </div>    
            </div>
                
        </div>
    </div>
</div>

@endsection