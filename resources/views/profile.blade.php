@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
    	<?php //print_r($user);exit; ?>
        @include('layouts.leftside')
        <div class="col-md-8">
        	<div class="panel panel-default"><br/>
        		<div class="panel-body">
        		<div class="row">
        			<div class="col-sm-6">Name:</div>
        			<div class="col-sm-6">{{ $user['name'] }}</div>
        		</div>
        		<div class="row">
        			<div class="col-sm-6">Email Id:</div>
        			<div class="col-sm-6">{{ $user['email'] }}</div>
        		</div>
        		<div class="row">
        			<div class="col-sm-8"></div>
        			<div class="col-sm-4"><a href="{{ route('editprofile') }}" class="btn btn-success">Edit My Profile</a></div>
        		</div><br/>
        	</div>
       		</div>
        </div>
        
    </div>
</div>
@endsection