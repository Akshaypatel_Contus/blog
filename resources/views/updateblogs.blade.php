@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        @include('layouts.leftside')
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    @foreach($blog as $key)
                    <form action="{{ route('blog_update') }}?id={{ $key->id }}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                    <div class="row">
                        <div class="col-sm-6">
                            Blog Title:
                        </div>
                        <div class="col-sm-6">
                            <input type="text" name="blog_title" class="form-control" value="{{ $key->blog_title }}" />
                            @if ($errors->has('blog_title'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('blog_title') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div><br/>
                    <div class="row">
                        <div class="col-sm-6">
                            Blog Description
                        </div>
                        <div class="col-sm-6">
                            <textarea name="blog_desc" class="form-control">{{ $key->blog_desc }}</textarea>
                            @if ($errors->has('blog_desc'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('blog_desc') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div><br/>
                    <div class="row">
                        <div class="col-sm-6">Blog Image Attachment</div>
                        <div class="col-sm-6">
                            <input type="file" name="blog_img" /><span style="color:red">*If change please upload again</span>
                            <?php 
                            //Storage::disk('local')->put('avatars/filename.jpg', $file);
                            //$file=Storage::disk('local')->url("app/".$key->blog_img);
                            $path = storage_path($key->blog_img);
                            $url = Storage::url($key->blog_img);
                             ?>
                            <img src="{{ asset($key->blog_img) }}" />
                            @if ($errors->has('blog_img'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('blog_img') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6"></div>
                        <div class="col-sm-6">
                            <button type="submit" name="submit" class="btn btn-success">Update My Blog</button>
                        </div>
                    </div>
                </form>
                @endforeach
                   


                </div>
            </div>
        </div>
    </div>
</div>
@endsection
