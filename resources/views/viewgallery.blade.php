@extends('layouts.app')

@section('content')
<?php use Illuminate\Support\Facades\Storage; ?>
<div class="container">
    <div class="row">
    	@include('layouts.leftside')
        <div class="col-md-8">
            
            <div class="panel panel-default">
                <div class="panel-heading">Gallery</div>
                <div class="panel-body">
                    @foreach($gallery as $key)
                    <div class="row">
                        <div class="col-sm-12">{{ $key->name }}</div>    
                    </div>
                    <div class="row">
                        @foreach(explode(',', $key->image) as $image)
                        <div class="col-sm-6">
                            <img src="{{ asset($image) }}" />
                        </div>
                        @endforeach
                    </div>
                    @endforeach
                </div>    
            </div>
                
        </div>
    </div>
</div>

@endsection