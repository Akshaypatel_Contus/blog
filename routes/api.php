<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




Route::get('api/blog', 'ApiController@blogs')->name('blog');
Route::get('api/delete','ApiController@deleteblog')->name('delete');
Route::get('api/edit','ApiController@editblog')->name('edit');
Route::post('api/blog_insert','ApiController@blog_insert')->name('blog_insert');
Route::post('api//blog_update','ApiController@blog_update')->name('blog_update');
Route::post('api/insertuser','ApiController@insertuser')->name('insertuser');