<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'UserController@index')->name('index');//index page
Route::get('/index', 'UserController@index')->name('index');//index page
Auth::routes();//login,register

Route::get('/blog', 'HomeController@blogs')->name('blog');//blog page
Route::get('/delete','HomeController@deleteblog')->name('delete');//delete blog page
Route::get('/edit','HomeController@editblog')->name('edit');//update blog page
Route::get('/home','HomeController@index')->name('home');//home page after login
Route::post('/blog_insert','HomeController@blog_insert')->name('blog_insert');//add blog
Route::post('/blog_update','HomeController@blog_update')->name('blog_update');//update blog
Route::get('/adduser','HomeController@adduser')->name('adduser');//add user page
Route::post('/insertuser','HomeController@insertuser')->name('insertuser');//add user
Route::get('/profile','HomeController@profile')->name('profile');//view profile
Route::get('/editprofile','HomeController@editprofile')->name('editprofile');//update profile page
Route::get('/multiupload','HomeController@multiupload')->name('multiupload');//multi upload page
Route::get('/viewgallery','HomeController@viewgallery')->name('viewgallery');//view gallery page
Route::post('/multi_upload_insert','HomeController@multi_upload_insert')->name('multi_upload_insert');//add gallery
Route::post('/update_profile','HomeController@update_profile')->name('update_profile');//update profile
Route::get('/blog_view', 'HomeController@blog_view')->name('blog_view');//view blog
Route::post('/load_comment', 'HomeController@load_comment')->name('load_comment');//comment load
Route::post('/blog_comment', 'HomeController@blog_comment')->name('blog_comment');//add comment
Route::post('/reply_comment', 'HomeController@reply_comment')->name('reply_comment');//replycomment  
Route::post('/load_reply', 'HomeController@load_reply')->name('load_reply');//reply comment
Route::post('/delete_reply', 'HomeController@delete_reply')->name('delete_reply');//delete reply comment
Route::post('/delete_cmt', 'HomeController@delete_cmt')->name('delete_cmt');//delete comment